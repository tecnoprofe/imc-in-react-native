import { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput,Image,Button} from 'react-native';

export default class App extends Component {
  constructor(props){
    super(props);
    
    this.state = {
      data: [],
      isLoading: true,
      altura:"1.7",
      peso:"70",
      imc:0,
      descripcion:""
    };
  }

  calculoIMC(){    
    let aux=(this.state.peso/(this.state.altura*this.state.altura)).toFixed(1)
    this.setState({imc:aux})
    if (aux<18.5){
      this.setState({descripcion:"Bajo Peso"})
    }

  }
  

  render() {
    return (
      <View style={styles.container}>
        <Text>IMC</Text>
        <StatusBar style="auto" />
        <View style={{flexDirection:'row',alignItems:'flex-start'}}>
          <TextInput
            style={styles.box1}
            onChangeText={(valor)=>this.setState({altura:valor})}
            value={this.state.altura}
          />
          <Text style={styles.box2}>m.</Text>
        </View>

        <View style={{flexDirection:'row',alignItems:'flex-start'}}>
          <TextInput 
            style={styles.box1}
            onChangeText={(valor)=>this.setState({peso:valor})}
            value={this.state.peso}
          />
          <Text style={styles.box2}>Kg.</Text>
        </View>

        <Button
          onPress={() => this.calculoIMC()}
          title="Calcular"
          color="#841584"          
        />

        <Text style={styles.box1}>
          {this.state.imc + this.state.descripcion}
        </Text>        

        <Image
          style={styles.logo}
          source={{uri: 'https://www.upds.edu.bo/wp-content/uploads/2020/10/upds_logo-1-1-1.png'}}          
        />
      </View>
    );
  }  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3fcaa3',        
  },
  box1:{
    fontSize:40,
    borderColor:'black',
    borderStyle:'solid',
    borderWidth:2,
    padding:10,    
    width: 200,
    textAlign: 'center',
    margin:10
  },
  box2:{
    fontSize:35,   
    padding:10,        
    textAlign: 'center',
    margin:10
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: 80,
    height: 80,
  },

});
